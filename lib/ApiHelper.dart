
import 'dart:convert';

import 'package:bloc_tutorial/post.dart';
import 'package:http/http.dart' as client;

class ApiHelper {
  
  Future<List<Post>> getList() async {
    try {
      final response = await client.get("https://jsonplaceholder.typicode.com/posts");
      var parsed = json.decode(response.body.toString());
      return (parsed as List).map((model) => Post.fromJson(model)).toList();
    } catch(e) {
      return List<Post>();
    }
  }

}