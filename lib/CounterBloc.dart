import 'dart:async';
import 'package:bloc_tutorial/ApiHelper.dart';
import 'package:bloc_tutorial/BlocProvider.dart';
import 'package:bloc_tutorial/post.dart';


class CounterBloc extends BlocBase{

  var _api = ApiHelper();
  
  StreamController streamController = StreamController<List<Post>>.broadcast();
  Stream<List<Post>> get listStream => streamController.stream;
  
  fetchData() async {
    var result = await _api.getList();
    streamController.sink.add(result);
  }

  @override
  void dispose() {
    streamController.close();
  }

}