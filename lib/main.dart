import 'package:bloc_tutorial/BlocProvider.dart';
import 'package:bloc_tutorial/CounterBloc.dart';
import 'package:bloc_tutorial/post.dart';
import 'package:flutter/material.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      bloc: CounterBloc(),
      child: MaterialApp(
        title: 'Flutter Demo',
        theme: ThemeData(
          primarySwatch: Colors.blue,
        ),
        home: MyHomePage(title: 'Flutter Demo Home Page'),
      ),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {

  @override
  void initState() {
    final CounterBloc bloc = BlocProvider.of<CounterBloc>(context);
    bloc.fetchData();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: _getStreamChild(),
    );
  }

  Widget _getStreamChild() {
    final CounterBloc bloc = BlocProvider.of<CounterBloc>(context);
    return StreamBuilder<List<Post>>(
      stream: bloc.listStream,
      builder: (BuildContext context, snapshot) {
        if(snapshot.hasData){
          return snapshot.data.length<1?
          Center(child: Text("Ga ono data e mbuut"))
          :ListView.builder(
            itemExtent: 100,
            itemCount: snapshot.data.length,
            itemBuilder: (context, index) {
              return Text(snapshot.data[index].title);
            },
          );
        }else if (snapshot.hasError){
          return Center(child: Text("Waduh, error cukk"));
        }
        return Center(child: CircularProgressIndicator());
      },
    );
  }
}
